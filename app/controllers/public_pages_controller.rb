class PublicPagesController < ApplicationController

	def index
		@title="IAC Home"
	end

	def aboutus
		@title=" About Us"
    end

    def productsandservices
    	@title="Products and Services"
    end

    def partners
    	@title="Partners"
    end

    def contactus
    	@title="Contact Us"
    end

    def casestudies
    	@title="Case Studies"
    end

    def news
    	@title="News"
    end

    def security
        @title="Security"
    end

    def markets
        @title="markets"
    end

    def careers
        @title="careers"
    end

end
